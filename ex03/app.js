// include express framework module
var express = require('express');
// include the node.js file system module
var fs = require('fs');

// set up new express app
var app = express();

// set app to listen on port 3000
app.set('port', process.env.PORT || 3000);

// specify that the app should use handlebars
var handlebars = require('express-handlebars');
// set handlebars as the app's view engine
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// when user navigates to http://localhost:3000/ render 'about' view (about.handlebars)
app.get('/', function (req, res) {

    var hobbies = ["chop wood", "knit sweaters", "collect stamps", "set out my outfits for the next working week", "cry"];

    // empty hobbies array for testing:
    // var hobbies = [];
    // null hobbies array for testing:
    // var hobbies = null;

    // if statement ensures random index selection methods do not cause error if hobbies is null
    if (hobbies != null) {
        var randomHobby = hobbies[Math.floor(Math.random() * hobbies.length)];
    } else {
        randomHobby = "Doing nothing";
    }

    var hobbyObj = {
        hobby: randomHobby,
        hobbiesArray: hobbies,
    }

    res.render('about', hobbyObj);
});

// allow all files in /public folder to be served to clients
app.use(express.static(__dirname + "/public"));

// render the 404 view if client tries to navigate to a route not handled in the above code
app.use(function(req, res, next) {
    res.status(404);
    res.render('404');
});

// render the 500 view if there is an internal server error
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});

// start the server running
app.listen(app.get('port'), function() {
    console.log('Express started on http://localhost:' + app.get('port'));
});